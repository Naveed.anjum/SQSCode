//============================================================================
// Name        : Middleware.cpp
// Author      : Naveed Anjum
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================
//===========================================================================
// Inlcude Libraries
//==========================================================================
#include <iostream>
#include<stdio.h>
#include<string.h>    //strlen
#include<stdlib.h>
#include <unistd.h>
#include <aws/core/Aws.h>
#include <aws/sqs/SQSClient.h>
#include <aws/sqs/model/SendMessageRequest.h>
#include <aws/sqs/model/SendMessageResult.h>
#include <aws/core/auth/AWSCredentialsProviderChain.h>
#include <aws/sqs/model/ReceiveMessageRequest.h>
#include <aws/sqs/model/DeleteMessageRequest.h>
#include <aws/core/Core_EXPORTS.h>
#include <aws/core/client/CoreErrors.h>
#include <aws/core/http/HttpTypes.h>
#include <aws/core/utils/memory/stl/AWSString.h>
#include <aws/core/AmazonWebServiceResult.h>
#include <aws/core/utils/crypto/Hash.h>
#include <memory>
#include <aws/sqs/model/CreateQueueRequest.h>
#include <aws/sqs/model/CreateQueueResult.h>
#include <aws/core/client/DefaultRetryStrategy.h>
#include <aws/sqs/model/DeleteQueueRequest.h>
#include <aws/sqs/model/GetQueueUrlRequest.h>
#include <aws/sqs/model/GetQueueUrlResult.h>
using namespace std;
int ReceiveMessage(Aws::String,Aws::String); // function declaration
int breakit=0;
using namespace Aws;
using namespace Aws::SQS::Model;
using namespace Aws::SQS;
Aws::String imagepath="/opt/camera APP/Visonsx"; // image path
void Deletequeue(Aws::String); // function declaration
//================================== Main Function ================================
int main(int argc, char *argv[]) {

	Aws::String queue_message;
	Aws::String final_message;
	Aws::String service_type;
	Aws::String queue_urlR;
	Aws::String uuid;
	Aws::String Queue_name;
	int ReturnValue=0;

	if ( argc != 3 ) // argc should be 3 for correct execution
	    // We print argv[0]Fassuming it is the program name
	    cout<<"usage: "<< argv[0] <<" <uuid servicetype>\n";
	  else
	  {
		  service_type=argv[2]; // service type
		  uuid=argv[1]; // uuid
		  Queue_name= uuid + "-" + service_type; // queue name
		  // queue url
		  Aws::String queue_url = "https://sqs.us-east-1.amazonaws.com/754887566034/Requests.fifo";
		  Aws::String msg_body ;
		  Aws::SDKOptions options; //  SDK options
		  Aws::InitAPI(options); // AWS SDK initialization
		  {

	    	Aws::Client::ClientConfiguration configuration;
	    	//configuration.region = Aws::Region::US_EAST_1;
	    	configuration.scheme = Aws::Http::Scheme::HTTPS; // connection scheme
	    	configuration.connectTimeoutMs = 30000; // connection time out in milli seconds
	    	configuration.requestTimeoutMs = 600000; // request time out in milli seconds
	    	Aws::SQS::SQSClient sqs{configuration}; // setting configuration
	    	Aws::SQS::Model::CreateQueueRequest cq_req; // create queue object
	    	cq_req.SetQueueName(Queue_name ); // setting queue name

	    	auto cq_out = sqs.CreateQueue(cq_req); // creating the queue
	    	if (cq_out.IsSuccess()) // if successfull
	    	{
	    	     std::cout << "Successfully created queue " << Queue_name << std::endl;

	    	} else
	    	{
	    	     std::cout << "Error creating queue " << Queue_name << ": " <<
	    	     cq_out.GetError().GetMessage() << std::endl;
	    	     return 1;
	    	}

	    	Aws::SQS::Model::GetQueueUrlRequest gqu_req; // get queue url request object
	    	gqu_req.SetQueueName(Queue_name); // set the object with queue name
	    	auto gqu_out = sqs.GetQueueUrl(gqu_req); // get the queue url
	    	if (gqu_out.IsSuccess()) // if successfull
	    	{
	    	    queue_urlR=gqu_out.GetResult().GetQueueUrl();
	    	    std::cout << "Queue " << Queue_name << " has url " <<
	    	    gqu_out.GetResult().GetQueueUrl() << std::endl;
	    	} else
	    	{
	    	    std::cout << "Error getting url for queue " << Queue_name << ": " <<
	    	    gqu_out.GetError().GetMessage() << std::endl;
	    	    return 1;
	        }

	    	Aws::SQS::Model::SendMessageRequest sm_req; // send message object
	    	sm_req.SetQueueUrl(queue_url); // set the queue for sending the message to
	    	queue_message=uuid + "$" + service_type + "$" + queue_urlR; // message structure
	    	msg_body=queue_message;
	    	sm_req.SetMessageBody(msg_body); // set the message
	    	sm_req.SetMessageGroupId("Visionx"); // group id
	    	auto sm_out = sqs.SendMessage(sm_req); // send the message
	    	if (sm_out.IsSuccess())// if successfull
	    	{
	    		 std::cout << "Successfully sent message to " << queue_url <<std::endl;
	    	} else
	    	{
	    		 std::cout << "Error sending message to " << queue_url << ": " <<
	    		                    sm_out.GetError().GetMessage() << std::endl;
	    	}

	    }//End of APIInit
		// inifinte while loop works as main thread
	    while(1)
	    {
	    	ReturnValue=ReceiveMessage(uuid,queue_urlR); // check the queue with uuid for return message
	    	if(breakit)
	    	break;
	    	sleep(1); // sleep for 1 second
	    }

	    Deletequeue(queue_urlR); // delete the queue
	    Aws::ShutdownAPI(options); // shutdown the AWS API
	  }//End of else

return ReturnValue;
}
//=============================== ReceiveMessage fuction =================================

int ReceiveMessage(Aws::String uuid,Aws::String  queue_urlS)
{
    // Let's make sure the request timeout is larger than the maximum possible
    // long poll time so that valid ReceiveMesage requests don't fail on long
    // poll queues
	int ReturnValue=0;
	Aws::Client::ClientConfiguration client_cfg;
    client_cfg.requestTimeoutMs = 30000; // request time out in milliseconds
    client_cfg.scheme = Aws::Http::Scheme::HTTPS; // connection type
    client_cfg.connectTimeoutMs = 30000; // connection timeout in milliseconds
    Aws::SQS::SQSClient sqs(client_cfg); // set the configuration
    Aws::SQS::Model::ReceiveMessageRequest rm_req; // receive message request object
    rm_req.SetQueueUrl(queue_urlS); // set the request with queue url
    rm_req.SetMaxNumberOfMessages(1);
    auto rm_out = sqs.ReceiveMessage(rm_req); // receive the message
    if (!rm_out.IsSuccess()) // if not successfull
    {
        std::cout << "Error receiving message from queue " << queue_urlS << ": "
            << rm_out.GetError().GetMessage() << std::endl;
        return 0;
    }
    const auto& messages = rm_out.GetResult().GetMessages(); // get the messages list
    if (messages.size() == 0) { // if no message
        std::cout << "No messages received from queue " << queue_urlS <<
            std::endl;
        return 0;
    }

    const auto& message = messages[0]; // list the first message
    std::cout << "Received message:" << std::endl;
    Aws:String messagebody=message.GetBody();// get message body
    std::cout << "  Body: " << messagebody << std::endl << std::endl;
    std::size_t pos = messagebody.find(":");      // position of ":" in message
    Aws::String status = messagebody.substr (0,pos);
    pos++;
    Aws::String receiveduuid = messagebody.substr (pos); // extract uuid from message
    int ss=uuid.compare(receiveduuid); // comparing the uuid with uuid extracted from message
    if (ss==0) // if comarison is successfull
    {
    	if (status.compare("Success")) // compare the status extracted from message
            ReturnValue=0;
        if (status.compare("Failure"))
            ReturnValue=1;

        breakit=1;
    }

 return ReturnValue;
}
//============================================= Delete queue =============================
void Deletequeue(Aws::String url)
{
	 Aws::Client::ClientConfiguration client_cfg;
	 //client_cfg.region = Aws::Region::US_EAST_1;
	 client_cfg.scheme = Aws::Http::Scheme::HTTPS;
	 client_cfg.connectTimeoutMs = 30000;
	 client_cfg.requestTimeoutMs = 600000;
	 client_cfg.retryStrategy =
	 Aws::MakeShared<Aws::Client::DefaultRetryStrategy>("sqs_delete_queue", 0);
	 Aws::SQS::SQSClient sqs1(client_cfg);
	 Aws::SQS::Model::DeleteQueueRequest dq_req; // delete queue request object
	 dq_req.SetQueueUrl(url); // set the url to delete the queue
	 auto dq_out = sqs1.DeleteQueue(dq_req); // delete the queue
	 if (dq_out.IsSuccess()) // if deletion successfull
	 {
		 std::cout << "Successfully deleted queue with url " << url <<std::endl;

	 } else
	 {
		 std::cout << "Error deleting queue " << url << ": " <<dq_out.GetError().GetMessage() << std::endl;

	 }
}
