//============================================================================
// Name        : Worker.cpp
// Author      : Naveed Anjum
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

//===========================================================================
// Inlcude Libraries
//==========================================================================
#include <iostream>
#include<stdio.h>
#include<string.h>    //strlen
#include<stdlib.h>
#include <unistd.h>
#include <aws/core/Aws.h>
#include <aws/sqs/SQSClient.h>
#include <aws/sqs/model/SendMessageRequest.h>
#include <aws/sqs/model/SendMessageResult.h>
#include <aws/core/auth/AWSCredentialsProviderChain.h>
#include <aws/sqs/model/ReceiveMessageRequest.h>
#include <aws/sqs/model/DeleteMessageRequest.h>
#include<pthread.h>
#include <aws/s3/S3Client.h>
#include <aws/s3/model/PutObjectRequest.h>
#include <aws/s3/model/GetObjectRequest.h>
#include <fstream>
#include <aws/s3/model/DeleteObjectRequest.h>
using namespace std;
using namespace Aws;
using namespace Aws::SQS::Model;
using namespace Aws::SQS;
void *message_handler(void *); // function declaration
void ReceiveMessage(void); // function declaration

//================================== Main Function ================================

int main() {

			pthread_t requests_thread;  // thread object
			int* new_msg;
			Aws::SDKOptions options;
			Aws::InitAPI(options); // initialize the APIs of AWS SDK
			cout<<"creating thread"<<endl;
		    // thread creation with message_handler as handler of running thread
			if( pthread_create( &requests_thread , NULL ,  message_handler , (void*) new_msg) < 0)
		    {
		        perror("could not create thread");
		        return 1;
		    }
		    //cout<<"end"<<endl;
			// inifinte while loop works as main thread
		    while(1)
		    {
		        sleep(1); // sleep the thread for one second
		    }

	return 0;
}

//================================= Running thread ================================

void *message_handler(void *msg_new)
{
	// thread loop
	while(1)
	{

    	ReceiveMessage(); // check for new message received in queue
    	sleep(1); // sleep for one second
	}


	return 0;

}

void ReceiveMessage()
{
    // Let's make sure the request timeout is larger than the maximum possible
    // long poll time so that valid ReceiveMesage requests don't fail on long
    // poll queues

	Aws::String filepath="/home/naveed/";
	Aws::String savepath="/opt/CameraApp/visionx/"; // path for image files
	Aws::String ocrpath="/opt/CameraApp/PriceRight2_1"; // path for ocr image files
	Aws::String nltkpath="/opt/CameraApp/nltk/"; // path for nltk image files
	const Aws::String bucket_name ="images.visionx" ; // sqs s3 bucket name used to save files

	int ApplicationReturn=-1; // application reutrn check
	Aws::String Success="Success";
	Aws::String Failure="Failure";
	Aws::Client::ClientConfiguration client_cfg; // configuration object
    client_cfg.requestTimeoutMs = 30000;  // request timout in milli seconds
    client_cfg.region = Aws::Region::US_EAST_1; // AWS region of SQS queue
    client_cfg.scheme = Aws::Http::Scheme::HTTPS; // connection scheme
    client_cfg.connectTimeoutMs = 30000; // connection timout in seconds
    Aws::SQS::SQSClient sqs(client_cfg); // sqs object
    Aws::String queue_urlS;
    Aws::String msg_body;
    // SQS queue url
    Aws::String queue_url = "https://sqs.us-east-1.amazonaws.com/754887566034/Requests.fifo";
    Aws::SQS::Model::ReceiveMessageRequest rm_req; // received message object
    rm_req.SetQueueUrl(queue_url); // set the current queue to the url
    rm_req.SetMaxNumberOfMessages(1); // setting maximum number of messages

    auto rm_out = sqs.ReceiveMessage(rm_req); // check the received message in queue
    if (!rm_out.IsSuccess()) { //  if not success
        std::cout << "Error receiving message from queue " << queue_url << ": "
            << rm_out.GetError().GetMessage() << std::endl;
        return;
    }

    const auto& messages = rm_out.GetResult().GetMessages(); // get the messages in the queue
    if (messages.size() == 0) { // if no message received
        std::cout << "No messages received from queue " << queue_url <<
            std::endl;
        return;
    }

    const auto& message = messages[0]; // access the first message in queue
    std::cout << "Received message:" << std::endl;
    Aws:String messagebody=message.GetBody(); // message body
    std::cout << "  Body: " << messagebody << std::endl;

    std::size_t pos = messagebody.find("$");      // finding $ in message
	Aws::String uuid = messagebody.substr (0,pos);     //  extract the uuid
	pos++;
	Aws::String servicetypeurl = messagebody.substr (pos); //  message without uuid
	std::size_t pos1 = servicetypeurl.find("$"); // extract the next $ sign in message
	Aws::String servicetype = servicetypeurl.substr (0,pos1); //  type of service
	pos1++;
	queue_urlS = servicetypeurl.substr (pos1); // extracting url of reply queue

    // Downloading

    const Aws::String key_name = uuid + ".png"; // key for message with uuid
    const Aws::String lKey=uuid + "-preprocessed.png"; //  preprocessed image
    const Aws::String Tessaract_lKey=uuid + "-preprocessed_out_omni.json"; // output of tessaract
    const Aws::String nltkoutkey=uuid + "-preprocessed_nltk.json"; // output of nltk
    const Aws::String file_namenltk=savepath + nltkoutkey; // nltk output image with path
    const Aws::String TessoutKey=uuid + "-preprocessed_out_ocrlines_word_wbb.json"; // tessaract output
    const Aws::String file_nameTess= savepath + uuid + "-preprocessed_out_ocrlines_word_wbb.json";
    const Aws::String file_name = savepath + uuid + "-preprocessed.png"; // file name with uuid and path
    Aws::S3::S3Client s3_client(client_cfg); // s3 bucket object



    Aws::SQS::Model::DeleteMessageRequest dm_req; // message delete request
    dm_req.SetQueueUrl(queue_url); // set the queue for deleting message
    dm_req.SetReceiptHandle(message.GetReceiptHandle()); // get the handle

    auto dm_out = sqs.DeleteMessage(dm_req); // delete the message in queue
    if (dm_out.IsSuccess()) // if success in deleting
    { // deletion if start
        std::cout << "Successfully deleted message " << message.GetMessageId()
            << " from queue " << queue_url << std::endl;

        if(servicetype=="OCR") // if service type is OCR
        { // OCR if
        		std::cout << "Downloading " << key_name << " from S3 bucket: " <<
        	    bucket_name << std::endl;
       	        Aws::S3::Model::GetObjectRequest object_request1; // s3 object
        	    object_request1.WithBucket(bucket_name).WithKey(key_name); // set the bucket with key name
        	    auto get_object_outcome = s3_client.GetObject(object_request1); // get the object
                if (get_object_outcome.IsSuccess()) { // if success then download the file at location supplied
        	        Aws::OFStream local_file;
        	        local_file.open((savepath + key_name).c_str(), std::ios::out | std::ios::binary);
        	        local_file << get_object_outcome.GetResult().GetBody().rdbuf();
                    std::cout << "Done!" << std::endl;

        	    } else { //  if download is not successfull
                    std::cout << "GetObject error: " <<
        	        get_object_outcome.GetError().GetExceptionName() << " " <<
        	        get_object_outcome.GetError().GetMessage() << std::endl;
        	    }

            	const char * cm="-v mason -p";
            	Aws::String ocr= ocrpath + " " + savepath + uuid + ".png " + cm; // command to execute ocr exe
				//Aws::String cm=ocr + " " + uuid;
				//const char* cmd=cm;
            	const char * cmd=ocr.c_str();
            	//sprintf(command,"%s", ocr);
            	ApplicationReturn=system(cmd); // execute the ocr exe

        }// OCR if end
        else if(servicetype=="NLTK") // if service type is NLTK
        { // NLTK if
        	std::cout << "Downloading " << TessoutKey << " from S3 bucket: " <<
            bucket_name << std::endl;
        	Aws::S3::Model::GetObjectRequest object_request1;
            object_request1.WithBucket(bucket_name).WithKey(TessoutKey);
            auto get_object_outcome = s3_client.GetObject(object_request1);
            if (get_object_outcome.IsSuccess())
            {
            	Aws::OFStream local_file;
            	local_file.open((savepath + TessoutKey).c_str(), std::ios::out | std::ios::binary);
                local_file << get_object_outcome.GetResult().GetBody().rdbuf();
                std::cout << "Done!" << std::endl;
            } else
            {
            	cout<<"word_wbb.json not found"<<endl;
            	std::cout << "GetObject error: " <<
            	get_object_outcome.GetError().GetExceptionName() << " " <<
				get_object_outcome.GetError().GetMessage() << std::endl;
            	return;
            }

            const char * cm="run-nltk.sh";
            // nltk exe string
            Aws::String ocr= savepath + cm + " " + savepath + uuid + "-preprocessed ";
            //Aws::String cm=ocr + " " + uuid;
            //const char* cmd=cm;
            const char * cmd=ocr.c_str();
            //sprintf(command,"%s", ocr);
            ApplicationReturn=system(cmd); // execute the exe
        }// NLTK if end
        else if(servicetype=="TESSARACT")
        { // TESSARACT if
        	std::cout << "Downloading " << lKey << " from S3 bucket: " <<
        	bucket_name << std::endl;
        	//Aws::S3::S3Client s3_client(client_cfg);
        	Aws::S3::Model::GetObjectRequest object_request1;
        	object_request1.WithBucket(bucket_name).WithKey(lKey);
        	auto get_object_outcome = s3_client.GetObject(object_request1);
        	if (get_object_outcome.IsSuccess())
        	{
        		Aws::OFStream local_file;
        		local_file.open((savepath + lKey).c_str(), std::ios::out | std::ios::binary);
        		local_file << get_object_outcome.GetResult().GetBody().rdbuf();
        		std::cout << "Done!" << std::endl;
        	} else
        	{
        		std::cout << "GetObject error: " <<
        		get_object_outcome.GetError().GetExceptionName() << " " <<
				get_object_outcome.GetError().GetMessage() << std::endl;
        	}
        	std::cout << "Downloading " << Tessaract_lKey << " from S3 bucket: " <<
        	bucket_name << std::endl;
        	object_request1.WithBucket(bucket_name).WithKey(Tessaract_lKey);
        	get_object_outcome = s3_client.GetObject(object_request1);
        	if (get_object_outcome.IsSuccess())
        	{
        		Aws::OFStream local_file;
        		local_file.open((savepath + Tessaract_lKey).c_str(), std::ios::out | std::ios::binary);
        		local_file << get_object_outcome.GetResult().GetBody().rdbuf();
        		std::cout << "Done!" << std::endl;
        	} else
        	{
        		cout<<"No omni json file in bucket"<<endl;
        		std::cout << "GetObject error: " <<
        		get_object_outcome.GetError().GetExceptionName() << " " <<
				get_object_outcome.GetError().GetMessage() << std::endl;
        		return;
        	}
        	const char * cm="run-all.sh";
        	// TESSARCT exection
        	Aws::String ocr= savepath + " " + cm + savepath + uuid + "-preprocessed";
        	const char * cmd=ocr.c_str();
        	ApplicationReturn=system(cmd); // execute command
        }// TESSARACT if end
        else
        {
        	cout<<"not valid";
        }
        Aws::SQS::Model::SendMessageRequest sm_req;
        sm_req.SetQueueUrl(queue_urlS);
        if(ApplicationReturn==0) // if aplication returned successfully
        {
        	// files returned from execution of different exe's should be available in s3 bucket
        	if(servicetype=="OCR")
        	{ // OCR if
        		Aws::S3::Model::PutObjectRequest object_request;
        		object_request.WithBucket(bucket_name).WithKey(lKey); // ocr output files with path
        		// Binary files must also have the std::ios_base::bin flag or'ed in
        		auto input_data = Aws::MakeShared<Aws::FStream>("PutObjectInputStream",
        		file_name.c_str(), std::ios_base::in);
        		object_request.SetBody(input_data);
        		auto put_object_outcome = s3_client.PutObject(object_request); // uploading in s3 bucket
        		if (put_object_outcome.IsSuccess())
        		{
        			std::cout << "Done!" << std::endl;
        		} else
        		{
        			std::cout << "PutObject error: " <<
        			put_object_outcome.GetError().GetExceptionName() << " " <<
					put_object_outcome.GetError().GetMessage() << std::endl;
        		}


        	} // OCR if end
        	if(servicetype=="TESSARACT")
        	{// TESSARACT if
        		Aws::S3::Model::PutObjectRequest object_request;
        		object_request.WithBucket(bucket_name).WithKey(TessoutKey); // Tessaract output files with path
        		// Binary files must also have the std::ios_base::bin flag or'ed in
        		auto input_data = Aws::MakeShared<Aws::FStream>("PutObjectInputStream",
        		file_nameTess.c_str(), std::ios_base::in);
        		object_request.SetBody(input_data);
        		auto put_object_outcome = s3_client.PutObject(object_request);
        		if (put_object_outcome.IsSuccess())
        		{
        			std::cout << "Done!" << std::endl;
        		} else
        		{
        			std::cout << "PutObject error: " <<
        			put_object_outcome.GetError().GetExceptionName() << " " <<
					put_object_outcome.GetError().GetMessage() << std::endl;
        		}


        	}// TESSARACT if end
        	if(servicetype=="NLTK")
        	{ // NLTK if
        		Aws::S3::Model::PutObjectRequest object_request;
        		object_request.WithBucket(bucket_name).WithKey(nltkoutkey);// nltk output files
        		// Binary files must also have the std::ios_base::bin flag or'ed in
        		auto input_data = Aws::MakeShared<Aws::FStream>("PutObjectInputStream",
        		file_namenltk.c_str(), std::ios_base::in);
        		object_request.SetBody(input_data);
        		auto put_object_outcome = s3_client.PutObject(object_request);
        		if (put_object_outcome.IsSuccess())
        		{
        			std::cout << "Done!" << std::endl;
        		} else
        		{
        			std::cout << "PutObject error: " <<
        			put_object_outcome.GetError().GetExceptionName() << " " <<
					put_object_outcome.GetError().GetMessage() << std::endl;
        		}


        	} // NLTK end
        	// message formation like success:uuid
        	msg_body=Success + ":" ;
        	msg_body+= uuid;
        	sm_req.SetMessageBody(msg_body); // setting the message object send with message
        	auto sm_out = sqs.SendMessage(sm_req); // send the message to return queue
        	if (sm_out.IsSuccess()) // if successfull send
        	{
        		std::cout << "Successfully sent message to " << queue_urlS <<std::endl;

            } else
            {
            	std::cout << "Error sending message to " << queue_url << ": " <<
            			sm_out.GetError().GetMessage() << std::endl;
            }

        }
        else
        {
        	// message formation like failure:uuid
        	msg_body= Failure + ":" ;
        	msg_body+= uuid;
        	sm_req.SetMessageBody(msg_body);
        	auto sm_out = sqs.SendMessage(sm_req);
        	if (sm_out.IsSuccess())
        	{
        		std::cout << "Successfully sent message to " << queue_urlS <<std::endl;

            } else
            {
            	std::cout << "Error sending message to " << queue_urlS << ": " <<
            	sm_out.GetError().GetMessage() << std::endl;

            }
        }

    } // deletion if ends
    else
    {
    	std::cout << "Error deleting message " << message.GetMessageId() <<
    	" from queue " << queue_url << ": " <<  dm_out.GetError().GetMessage() << std::endl;
    }


}// End of function
